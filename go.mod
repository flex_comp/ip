module gitlab.com/flex_comp/ip

go 1.16

require (
	github.com/ip2location/ip2location-go v8.3.0+incompatible
	gitlab.com/flex_comp/comp v0.1.3
	gitlab.com/flex_comp/conf v0.0.0-20210729190352-7eb0a9e754bc
	gitlab.com/flex_comp/log v0.0.0-20210729190650-519dfabf348e
	gitlab.com/flex_comp/util v0.0.0-20210730230013-7a585b221a6c
)
