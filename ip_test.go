package ip_lib

import (
	"gitlab.com/flex_comp/comp"
	"net"
	"testing"
	"time"
)

func TestIP_Country(t *testing.T) {
	_ = comp.Init(map[string]interface{}{"config": "./res/server.json"})

	ch := make(chan bool, 1)
	go func() {
		after := time.After(time.Second * 10)
		ticker := time.NewTicker(time.Second)

		defer func() {
			ticker.Stop()
			ch <- true
		}()

		ip := net.ParseIP("86.97.65.202")
		t.Log(IPLib.Country(ip))

		for {
			select {
			case <-after:
				return
			case <-ticker.C:
				now := time.Now()
				for i := 0; i < 100; i++ {
					IPLib.Country(ip)
				}
				t.Log("100次用时:", time.Now().Sub(now))
			}
		}
	}()

	_ = comp.Start(ch)
}
