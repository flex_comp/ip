package ip_lib

import (
	"github.com/ip2location/ip2location-go"
	"gitlab.com/flex_comp/comp"
	"gitlab.com/flex_comp/conf"
	"gitlab.com/flex_comp/log"
	"gitlab.com/flex_comp/util"
	"net"
)

var (
	IPLib *IP
)

type IP struct {
	ipDB *ip2location.DB
}

func init() {
	IPLib = new(IP)
	_ = comp.RegComp(IPLib)
}

func (i *IP) Init(_ map[string]interface{}, _ ...interface{}) error {
	ipPath := util.ToString(conf.Get("ip.path"))

	var err error
	i.ipDB, err = ip2location.OpenDB(ipPath)
	if err != nil {
		return err
	}

	return nil
}

func (i *IP) Start(_ ...interface{}) error {
	return nil
}

func (i *IP) UnInit() {

}

func (i *IP) Name() string {
	return "ip"
}

func Country(ip net.IP) string {
	return IPLib.Country(ip)
}

func (i *IP) Country(ip net.IP) string {
	rep, err := i.ipDB.Get_country_short(ip.String())
	if err != nil {
		log.Warn("未找到指定IP所属国家:", ip)
		return ""
	}

	return rep.Country_short
}
